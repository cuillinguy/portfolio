import { RouteConfig } from 'vue-router'

const routes: RouteConfig[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { 
        path: '', 
        component: () => import('pages/LandingPage.vue')
      },
      { path: '/home', component: () => import('pages/LandingPage.vue') },
      { path: '/ourvalues', component: () => import('pages/AboutUs.vue') },
      { path: '/:lang/blogs/:slug', component: () => import('pages/BlogDetail.vue') },
      { path: '/:lang/bloglist', component: () => import('pages/BlogList.vue') },
      { path: '/:lang/staff', component: () => import('pages/Staff.vue') },
      { path: '/:lang/contactus', component: () => import('pages/ContactUs.vue') },
      { path: '/:lang/quote', component: () => import('pages/GetAQuote.vue') },
      { path: '/:lang/contractors', component: () => import('pages/Contractors/Contractors.vue') },
      { path: '/:lang/contractorsupply', component: () => import('pages/Staffing/ContractorSupply.vue') },
      { path: '/:lang/products/:slug', component: () => import('pages/Products/ProductDetails.vue') },
      { path: '/:lang/rates/:slug', component: () => import('pages/OurRates/OurRates.vue') },
      { path: '/:lang/general/:slug', component: () => import('pages/General/GeneralContent.vue') },
      { path: '/:lang/privacy', component: () => import('pages/Footer/Privacy.vue') },
      { path: '/:lang/terms', component: () => import('pages/Footer/Terms.vue') },
      { 
        path: '/landingpage', 
        component: () => import('pages/LandingPage.vue')
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
