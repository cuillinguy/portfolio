// Firebase App (the core Firebase SDK) is always required and must be listed first
import * as firebase from 'firebase/app'

// If you enabled Analytics in your project, add the Firebase SDK for Analytics
import 'firebase/analytics'

// Add the Firebase products that you want to use
import 'firebase/auth'
import 'firebase/database'
import 'firebase/firestore'
import 'firebase/performance'
// import 'firebase/messaging' removed for now

const firebaseConfig = {
    // eslint-disable-next-line quotes
    apiKey: "AIzaSyAvDyCfW1Qb5Fc04H5Ev4aukrSIJRK4NiY",
    // eslint-disable-next-line quotes
    authDomain: "brdportfolio-754e1.firebaseapp.com",
    // eslint-disable-next-line quotes
    databaseURL: "https://brdportfolio-754e1.firebaseio.com",
    // eslint-disable-next-line quotes
    projectId: "brdportfolio-754e1",
    // eslint-disable-next-line quotes
    storageBucket: "brdportfolio-754e1.appspot.com",
    // eslint-disable-next-line quotes
    messagingSenderId: "716991125964",
    // eslint-disable-next-line quotes
    appId: "1:716991125964:web:6fa1ef1841875d0690e569",
    // eslint-disable-next-line quotes
    measurementId: "G-HFH5M99TBC"
    }
    // Initialize Firebase
    // const firebaseApp = firebase.initializeApp(firebaseConfig)

    // From Example
    export const firebaseApp = firebase
    .initializeApp(firebaseConfig)
    .firestore()

    // Enable offline support
    // https://dev.to/paco_ita/break-the-cache-api-limits-in-our-pwa-oo3
    firebase.firestore().enablePersistence()
    .catch(function (err) {
        // eslint-disable-next-line eqeqeq
        if (err.code == 'unimplemented') {
            // The current browser does not support all of the
            // features required to enable persistence
        }
    })

    const firebaseAuth = firebaseApp.auth()
    const firebaseDb = firebaseApp.database()
    const firestoreDb = firebase.firestore()
    const firebaseRoot = firebase
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const perf = firebase.performance()

    firebase.analytics()

    export { firebaseAuth, firebaseDb, firestoreDb, firebaseRoot }
