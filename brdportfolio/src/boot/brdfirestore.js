// Firebase App (the core Firebase SDK) is always required and must be listed first
// eslint-disable-next-line quotes
import * as firebase from "firebase/app"

// If you enabled Analytics in your project, add the Firebase SDK for Analytics
// eslint-disable-next-line quotes
import "firebase/analytics"

// Add the Firebase products that you want to use
// eslint-disable-next-line quotes
import "firebase/auth"
// eslint-disable-next-line quotes
import "firebase/database"
// eslint-disable-next-line quotes
import "firebase/firestore"

const firebaseConfig = {
    // eslint-disable-next-line quotes
    apiKey: "AIzaSyAvDyCfW1Qb5Fc04H5Ev4aukrSIJRK4NiY",
    // eslint-disable-next-line quotes
    authDomain: "brdportfolio-754e1.firebaseapp.com",
    // eslint-disable-next-line quotes
    databaseURL: "https://brdportfolio-754e1.firebaseio.com",
    // eslint-disable-next-line quotes
    projectId: "brdportfolio-754e1",
    // eslint-disable-next-line quotes
    storageBucket: "brdportfolio-754e1.appspot.com",
    // eslint-disable-next-line quotes
    messagingSenderId: "716991125964",
    // eslint-disable-next-line quotes
    appId: "1:716991125964:web:6fa1ef1841875d0690e569",
    // eslint-disable-next-line quotes
    measurementId: "G-HFH5M99TBC"
    }
    // Initialize Firebase
     const firebaseApp = firebase.initializeApp(firebaseConfig).firestore()

    // const firebaseApp = firebase.initializeApp({ projectId: 'ghanabrdapp' }).firestore()

    const firedb = firebaseApp

    export { firedb }
