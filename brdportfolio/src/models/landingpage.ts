import api from '../api/api'
// 2bKFSLAYUKw8uvcjRrVou2
export const HOME = '2bKFSLAYUKw8uvcjRrVou2'

export class LandingPage {
    id: null
    h1: string
    h2: string
    mainParagraph: string
    constructor ({
      id = null,
      h1 = '',
      h2 = '',
      mainParagraph = ''
    } = {}) {
      this.id = id
      this.h1 = h1
      this.h2 = h2
      this.mainParagraph = mainParagraph
    }
  }

  export function responseAdapter ({ fields, sys }) {
    return new LandingPage({ ...fields, ...sys })
  }
  export default {
    async get (id) {
      return responseAdapter(await api.getEntry(id))
    }
}
