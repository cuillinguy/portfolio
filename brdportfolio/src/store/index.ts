import { store } from 'quasar/wrappers'
import Vuex from 'vuex'
import main, { MAIN_MODULE } from './main-module'
import landingpage, { LANDINGPAGE_MODULE } from './landingpage'
import nav, { NAV_MODULE } from './nav-module'
// import firestoreModule from './fire-generic-module'
// import { AnyMxRecord } from 'dns'
import createPersistedState from 'vuex-persistedstate'
import { vuexfireMutations } from 'vuexfire'

export interface StoreInterface {
  // Define your own store structure, using submodules if needed
  // example: ExampleStateInterface;
  // Declared as unknown to avoid linting issue. Best to strongly type as per the line above.
  example: unknown;
}

export default store(function ({ Vue }) {
  Vue.use(Vuex)

  const Store = new Vuex.Store<StoreInterface>({
    modules: {
      // example
      [NAV_MODULE]: nav,
      [MAIN_MODULE]: main,
      [LANDINGPAGE_MODULE]: landingpage
      // firestoreModule
    },
    plugins: [createPersistedState()],

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: !!process.env.DEV,
    mutations: {
      ...vuexfireMutations
    }
  })

  return Store
})
