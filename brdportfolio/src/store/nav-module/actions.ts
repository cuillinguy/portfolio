import { ActionTree } from 'vuex'
import { StoreInterface } from '../index'
import state, { StateInterface } from './state'
import StoryblokClient from 'storyblok-js-client'

const actions = {

    loadSettings ({ commit }, context) {

        const token = 'QgNPvh0TZIMuvUAgWww3vwtt'
        const storyapi = new StoryblokClient({
            accessToken: token
          }) 
        console.log('firing loadsettings action')
        
        return storyapi.get(`cdn/stories/${context}/settings`, {
          version: context.version
        }).then((res) => {
          commit('setSettings', res.data.story.content)
        })
      }

}
export default actions
