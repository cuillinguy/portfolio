import { MutationTree } from 'vuex'
import Vue from 'vue'
import { StateInterface } from './state'

const mutation: MutationTree<StateInterface> = {
    someMutation (state: StateInterface) {
      // your code
      // state.
    },
    setSettings (state, settings) {
      state.settings = settings
    },
    setLanguage (state, language) {
      state.language = language
    },
    setCacheVersion (state, version) {
      state.cacheVersion = version
    }
}
export default mutation
