export interface StateInterface {
  cacheVersion: '';
  language: 'en';
  settings: {
    // eslint-disable-next-line camelcase
    main_navi: [];
  };
    prop: boolean;
      userOrders: {
          orderDetails: Array<{
            OrderId: string;
            AddressFirstLine: string;
            AddressSecondLine: string;
            AddressThirdLine: string;
            PostCode: string;
            ContactEmail: string;
            ContactPhone: string;
            DeliveryNote: string;
          }>;
      }; 
  }
  
  const state: StateInterface = {
  cacheVersion: '',
  language: 'en',
  settings: {
    // eslint-disable-next-line camelcase
    main_navi: []
  },
    prop: false,
      userOrders: {
        orderDetails: Array<{
            OrderId: '';
            AddressFirstLine: '';
            AddressSecondLine: '';
            AddressThirdLine: '';
            PostCode: '';
            ContactEmail: '';
            ContactPhone: '';
            DeliveryNote: '';
        }>()
      }
  }
  
  export default state
