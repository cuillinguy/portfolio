import { Module } from 'vuex'
import { StoreInterface } from '../index'
import state, { StateInterface } from './state'
import actions from './actions'
import getters from './getters'
import mutations from './mutations'

export const LANDINGPAGE_MODULE = 'landingpage'

const module: Module<StateInterface, StoreInterface> = {
  namespaced: true,
  actions,
  getters,
  mutations,
  state
}

export default module
