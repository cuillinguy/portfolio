import { MutationTree } from 'vuex'
import Vue from 'vue'
import { StateInterface } from './state'
// import state from '../products-module/state'

const mutation: MutationTree<StateInterface> = {
  someMutation (state: StateInterface) {
    // your code
    // state.
  },
  addOrder (state: StateInterface, payload) {
    // Fired AddOrderDetails Mutation 
    console.log('fired add order details mutation OrderID ' + payload.orderId)
     Vue.set(state.userOrders.orderDetails, payload.orderId, payload.orderDetails)  
    // state.userOrders.orderDetails.push(payload.orderDetails) 
  }
}

export default mutation
