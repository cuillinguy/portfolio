import landingPageModel, { LandingPage } from '../../models/landingpage'
export interface StateInterface {
    prop: boolean;
      userOrders: {
          orderDetails: Array<{
            OrderId: string;
            AddressFirstLine: string;
            AddressSecondLine: string;
            AddressThirdLine: string;
            PostCode: string;
            ContactEmail: string;
            ContactPhone: string;
            DeliveryNote: string;
          }>;
      }; 
  }
  
  const state: StateInterface = {
    prop: false,
      userOrders: {
        orderDetails: Array<{
            OrderId: '';
            AddressFirstLine: '';
            AddressSecondLine: '';
            AddressThirdLine: '';
            PostCode: '';
            ContactEmail: '';
            ContactPhone: '';
            DeliveryNote: '';
        }>()
      }
  }
  
  export default state
