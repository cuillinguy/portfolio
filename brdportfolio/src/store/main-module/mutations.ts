import { MutationTree } from 'vuex'
import Vue from 'vue'
import { StateInterface } from './state'

const mutation: MutationTree<StateInterface> = {
    someMutation (state: StateInterface) {
      // your code
      // state.
    }
}
export default mutation
