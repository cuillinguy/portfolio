import Vue from 'vue'
// 1. Require the Storyblok client
import StoryblokClient from 'storyblok-js-client'
// 2. Set your token - you will be able to create a new space later.
const token = 'QgNPvh0TZIMuvUAgWww3vwtt'

// 3. Initialize the client with the preview token so we can access our API easily
// from your space dashboard at https://app.storyblok.com
const storyapi = new StoryblokClient({
  accessToken: token
}) 

export default {
    data () {
        return {
          story: {},
          loading: false
        }
      },
      methods: {
        loadStory (version) {
          this.loading = true
          this.$storyblok.get({
            slug: this.slug,
            version: version
          }, (data) => {
            this.story = {
              content: {}
            }
            this.$nextTick(() => {
              this.story = data.story
              this.loading = false
            })
          }, (error) => {
            this.loading = false
            console.log(error)
          })
        },
        getStory (slug, version) {
            storyapi.get('cdn/stories/' + slug, {
             version: version
           })
           .then((response) => {
             
             this.$nextTick(() => {
               console.log('story data story ' + response.data.story)
               console.log('story data ' + response.data)
               console.log('slug ' + slug)
               console.log('version ' + version)
               console.log('content ' + response.data.story.content)
               this.story = response.data.story
             })
           })
           .catch((error) => {
             console.log(error)
           })
         }
      },
      created: function () {

        this.$storyblok.init({
          accessToken: 'QgNPvh0TZIMuvUAgWww3vwtt'
        })
        // get the slug id
        const slug = this.$route.params.slug
        console.log('fired mixin sblok')
        console.log('slug ' + slug)
        
        this.$storyblok.on('change', () => {
          this.getStory(slug, 'draft')
        })
        this.$storyblok.on('published', () => {
          this.getStory(slug, 'draft')
        })
    
        this.$storyblok.pingEditor(() => {
          this.getStory(slug, this.$storyblok.inEditor ? 'draft' : 'published')
        })
      }
}
