import * as functions from 'firebase-functions'

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

export const sendEmailCall = functions.https.onCall((data) => {
  const text = data.text
  return 'success: ' + text
})

export const emailAlert = functions.https.onRequest((request, response) => {

    response.set('Access-Control-Allow-Origin', '*')

        // if (request.method === 'POST') {
        // Send response to OPTIONS requests
        response.set('Access-Control-Allow-Methods', 'GET')
        response.set('Access-Control-Allow-Headers', 'Content-Type')
        response.set('Access-Control-Max-Age', '3600')        

        try {
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        const sgMail = require('@sendgrid/mail')
        sgMail.setApiKey('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')

        const data = request.body
        console.log('This is the data ' + data)
        console.log('This is the email' + data.email)

        let htmlMessage = '<html><head></head><body>'
            htmlMessage += '<p>You received a query from ' + data.fullname + '</p>'
            htmlMessage += '<p>This is coming from the function</p>'
            htmlMessage += '<p>Their email address was given as: ' + data.email + '</p>'
            htmlMessage += '<p>Phone number: ' + data.phonenumber + '</p>'

            htmlMessage += '<p>The message was: <br>'
            htmlMessage += data.message
            htmlMessage += '</p>'
    
        const msg = {
            to: 'cuillinguy@gmail.com',
            from: 'info@giannadart.com',
            subject: 'Enquiry Message from function',
            text: htmlMessage,
            html: htmlMessage
          }
    
        sgMail.send(msg)

        // This should be cuillinguy
        const fromEmail = data.email
        console.log('From Email should be cuillinguy@outlook.com ' + fromEmail)

        // Send Confirmation email with template
        // eslint-disable-next-line @typescript-eslint/no-use-before-define
         sendConfirmation(fromEmail, data.fullname)

        response.status(204).send('The email fired')
        } catch (error) {
          console.log('Error in Function to send email' + error)
        }
 })

 function sendConfirmation (to: string, fullname: string) {
    try {

      // eslint-disable-next-line @typescript-eslint/no-var-requires
      const sgMail = require('@sendgrid/mail')
      sgMail.setApiKey('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')

      const msg = {
        to: to,
        from: 'info@giannadart.com',
        subject: 'Confirmation Email',
        templateId: 'd-3c7d37d227cd4ce8a84d421a4960ff51',
        text: 'text example',
        html: 'html example',
        // eslint-disable-next-line @typescript-eslint/camelcase
        dynamic_template_data: {
          fullname: fullname
        }
      }

    sgMail.send(msg)
    
    } catch (err) {
      console.log('send confirmation error ' + err)  
    }
    console.log('sending confirm email')
 }
