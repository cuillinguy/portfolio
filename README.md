# README #

Code for Portfolio website: https://www.blackskye.app/

### Summary ###

* Hosted in Firebase GCP
* Version 2

### How do I get set up? ###

* Clone from Master
* Configuration - Quasar Dev to start, Quasar Build to build deployment files
* Dependencies
* Database configuration - N/A
* How to run tests - None exist
* Deployment instructions - Terminal firebase deploy --only hosting

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact